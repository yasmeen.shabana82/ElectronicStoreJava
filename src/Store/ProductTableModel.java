package Store;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

public class ProductTableModel extends AbstractTableModel
{
private final ArrayList<Product> productList;
     
    private final String[] columnNames = new String[] 
    {
      "Product Name", "Product Price", "In Stock"
    };
    
    private final Class[] columnClass = new Class[] 
    {
        String.class, double.class, String.class
    };
 
    public ProductTableModel(ArrayList<Product> productList)
    {
        this.productList = productList;
    }
     
    @Override
    public String getColumnName(int column)
    {
        return columnNames[column];
    }
 
    @Override
    public Class<?> getColumnClass(int columnIndex)
    {
        return columnClass[columnIndex];
    }
    
    public void addRow(Product rowData)
    {
        productList.add(rowData);
        fireTableRowsInserted(productList.size() - 1, productList.size() - 1);
    }
 
    @Override
    public int getColumnCount()
    {
        return columnNames.length;
    }
 
    @Override
    public int getRowCount()
    {
        return productList.size();
    }
 
    @Override
    public Object getValueAt(int rowIndex, int columnIndex)
    {
      Product row = productList.get(rowIndex);
      if(0 == columnIndex) 
      {
        return row.getProductName();
        }
        else if(1 == columnIndex) 
        {
            return row.getProductPrice();
        }
        else if(2 == columnIndex) 
        {
            return row.getInStock();
        }
        return null;
    }
    
   @Override
   public void setValueAt(Object aValue, int rowIndex, int columnIndex)
   {
    Product row = productList.get(rowIndex);
    if(0 == columnIndex) 
    {
         row.setProductName((String) aValue);
     }
     else if(1 == columnIndex) 
     {
         row.setProductPrice((double) aValue);
     }
     else if(2 == columnIndex) 
     {
         row.setInStock((String) aValue);
     }
   }
   
  @Override
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
      return true;
  }
}
