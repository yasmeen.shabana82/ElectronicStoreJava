package Store;

import java.util.Objects;

public class Product
{
  // variables declaration
  protected String productName;
  protected double productPrice;
  protected String inStock;
  
  public Product(String productName, double productPrice, String inStock)
    {     
        this.productName = productName;
        this.productPrice = productPrice;
        this.inStock = inStock;
    }
  public String getProductName()
  {
      return productName;
  }
  
  public void setProductName(String productName)
  {
    if(this.productName == null ? productName != null : !this.productName.equals(productName))
    {
      this.productName = productName;
    }
  }
  
  public double getProductPrice()
  {
      return productPrice;
  }
  
  public void setProductPrice(double productPrice)
  {
    if(!Objects.equals(this.productPrice, productPrice))
    {
      this.productPrice = productPrice;
    }
  }
  
  public String getInStock()
  {
      return inStock;
  }
  
  public void setInStock(String inStock)
  {
    if(this.inStock == null ? inStock != null : !this.inStock.equals(inStock))
    {
      this.inStock = inStock;
    }
  }
}
