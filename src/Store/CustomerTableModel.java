package Store;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

public class CustomerTableModel extends AbstractTableModel
{
  private final ArrayList<Customer> customerList;
     
    private final String[] columnNames = new String[] 
    {
      "ID", "First Name", "Last Name", "Address", "VIP Customer"
    };
    
    private final Class[] columnClass = new Class[] 
    {
        Integer.class, String.class, String.class, String.class, String.class
    };
 
    public CustomerTableModel(ArrayList<Customer> customerList)
    {
        this.customerList = customerList;
    }
     
    @Override
    public String getColumnName(int column)
    {
        return columnNames[column];
    }
 
    @Override
    public Class<?> getColumnClass(int columnIndex)
    {
        return columnClass[columnIndex];
    }
        
    public void addRow(Customer rowData)
    {
      if (rowData == null) 
      {
       throw new IllegalArgumentException("row Data cannot be null.");
      }
      fireTableRowsInserted(customerList.size() - 1, customerList.size() - 1);
    }

    @Override
    public int getColumnCount()
    {
        return columnNames.length;
    }
 
    @Override
    public int getRowCount()
    {
        return customerList.size();
    }
 
    @Override
    public Object getValueAt(int rowIndex, int columnIndex)
    {
      Customer row = customerList.get(rowIndex);
      if(0 == columnIndex) 
      {
        return row.getId();
        }
        else if(1 == columnIndex) 
        {
            return row.getFirstName();
        }
        else if(2 == columnIndex) 
        {
            return row.getLastName();
        }
        else if(3 == columnIndex) 
        {
            return row.getAddress();
        }
        else if(4 == columnIndex) 
        {
            return row.getVIP();
        }
        return null;
    }
    
   @Override
   public void setValueAt(Object aValue, int rowIndex, int columnIndex)
   {
    Customer row = customerList.get(rowIndex);
    if(0 == columnIndex) 
    {
         row.setId((Integer) aValue);
     }
     else if(1 == columnIndex) 
     {
         row.setFirstName((String) aValue);
     }
     else if(2 == columnIndex) 
     {
         row.setLastName((String) aValue);
     }
     else if(3 == columnIndex) 
     {
         row.setAddress((String) aValue);
     }
     else if(4 == columnIndex) 
     {
         row.setVIP((String) aValue);
     }
   }
   
   @Override
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
      return true;
  }
}
