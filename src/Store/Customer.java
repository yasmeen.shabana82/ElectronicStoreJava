package Store;

import java.util.Objects;

public class Customer
{
  protected int id;
  protected String firstName;
  protected String lastName;
  protected String address;
  protected String vip;
  
  public Customer(int id, String firstName, String lastName, String address, String vip)
    {     
        this.id = id;
        this.firstName = firstName;   
        this.lastName = lastName;
        this.address = address;
        this.vip = vip;
    }
  
  public int getId()
  {
      return id;
  }
  
  public void setId(int id)
  {
    if(this.id != id)
    {
      this.id = id;
    }
  }
  
  public String getFirstName()
  {
      return firstName;
  }
  
  public void setFirstName(String firstName)
  {
    if(this.firstName == null ? firstName != null : !this.firstName.equals(firstName))
    {
      this.firstName = firstName;
    }
  }
  
  public String getLastName()
  {
      return lastName;
  }
  
  public void setLastName(String lastName)
  {
    if(this.lastName == null ? lastName != null : !this.lastName.equals(lastName))
    {
      this.lastName = lastName;
    }
  }
  
  public String getAddress()
  {
      return address;
  }
  
  public void setAddress(String address)
  {
    if(this.address == null ? address != null : !this.address.equals(address))
    {
      this.address = address;
    }
  }
  
  public String getVIP()
  {
      return vip;
  }
  
  public void setVIP(String vip)
  {
    if(this.vip == null ? vip != null : !this.vip.equals(vip))
    {
      this.vip = vip;
    }
  }
}